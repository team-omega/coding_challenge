CREATE DATABASE Crime_management 
CREATE TABLE Crime (
 CrimeID INT PRIMARY KEY,
 IncidentType VARCHAR(255),
 IncidentDate DATE,
 CrimeLocation VARCHAR(255),
 Description TEXT,
 Status VARCHAR(20)
)----inserting data into Crime tableINSERT INTO Crime (CrimeID, IncidentType, IncidentDate, CrimeLocation, Description, Status)
VALUES
 (1, 'Robbery', '2023-09-15', '123 Main St, Cityville', 'Armed robbery at a convenience store', 'Open'),
 (2, 'Homicide', '2023-09-20', '456 Elm St, Townsville', 'Investigation into a murder case', 'UnderInvestigation'),
 (3, 'Theft', '2023-09-10', '789 Oak St, Villagetown', 'Shoplifting incident at a mall', 'Closed') INSERT INTO Crime(CrimeID, IncidentType, IncidentDate, CrimeLocation, Description, Status) VALUES(4,'Robbery','2023-09-17','452 main st,downtown','armed robbery at dmart','open')CREATE TABLE Victim (
 VictimID INT PRIMARY KEY,
 CrimeID INT,
 VictimName VARCHAR(255),
 ContactInfo VARCHAR(255),
 Injuries VARCHAR(255),
 FOREIGN KEY (CrimeID) REFERENCES Crime(CrimeID)
)
---inserting data into victim table
INSERT INTO Victim (VictimID, CrimeID, VictimName, ContactInfo, Injuries)
VALUES
 (1, 1, 'John Doe', 'johndoe@example.com', 'Minor injuries'),
 (2, 2, 'Jane Smith', 'janesmith@example.com', 'Deceased'),
 (3, 3, 'Alice Johnson', 'alicejohnson@example.com', 'None') INSERT INTO Victim(VictimID, CrimeID, VictimName, ContactInfo, Injuries) VALUES (4, 4, 'Venky Smith','venky@example.com','None')
CREATE TABLE Suspect (
 SuspectID INT PRIMARY KEY,
 CrimeID INT,
 Name VARCHAR(255),
 Description TEXT,
 CriminalHistory TEXT,
 FOREIGN KEY (CrimeID) REFERENCES Crime(CrimeID)
)
---inserting data into suspect table
INSERT INTO Suspect (SuspectID, CrimeID, Name, Description, CriminalHistory)
VALUES
 (1, 1, 'Robber 1', 'Armed and masked robber', 'Previous robbery convictions'),
 (2, 2, 'Unknown', 'Investigation ongoing', NULL),
 (3, 3, 'Suspect 1', 'Shoplifting suspect', 'Prior shoplifting arrests') INSERT INTO Suspect(SuspectID, CrimeID, Name, Description, CriminalHistory) VALUES(4, 4, 'Robber1','armed and masked robber',NULL) ---1. SELECT ALL OPEN INCIDENTS SELECT * FROM Crime WHERE Status = 'Open'

 ---2.Find the total number of incidents
SELECT COUNT(*) AS TotalIncidents FROM Crime

---3.List all unique incident types.
SELECT DISTINCT IncidentType FROM Crime

---4. Retrieve incidents that occurred between '2023-09-01' and '2023-09-10'.
SELECT * FROM Crime WHERE IncidentDate BETWEEN '2023-09-01' AND '2023-09-10'

---5.List persons involved in incidents in descending order of age.
--ADDING AGE COLUMN TO SUSPECT TABLE
---ALTER TABLE Suspect
---ADD AGE INT
--ADDING AGE COLUMN TO VICTIM TABLE
---ALTER TABLE Victim
---ADD AGE INT
--EXEC sp_rename 'Victim.AGE', 'VICTIMAGE'

SELECT v.VictimName,v.VICTIMAGE,s.AGE,s.Name [SUSPECT NAME]
FROM Victim v
JOIN  Suspect s ON v.CrimeID =s.CrimeID
ORDER BY v.VICTIMAGE DESC


---6.Find the average age of persons involved in incidents.
SELECT AVG(VICTIMAGE) [VICITIMS AVERAGE AGE],AVG(AGE) [SUSPECTS AVERAGE AGE] 
FROM Victim v
JOIN Suspect s ON v.CrimeID=s.CrimeID


---7. List incident types and their counts, only for open cases.
SELECT *FROM Crime
SELECT IncidentType, COUNT(*) AS IncidentCount FROM Crime WHERE Status = 'Open' GROUP BY IncidentType

--8. Find persons with names containing 'Doe'.SELECT * FROM Victim WHERE VictimName LIKE '%Doe%'

--9. Retrieve the names of persons involved in open cases and closed cases.
--10. List incident types where there are persons aged 30 or 35 involved.
SELECT DISTINCT c.IncidentType
FROM Crime c
JOIN Victim v ON c.CrimeID = v.CrimeID
JOIN Suspect s ON v.CrimeID =s.CrimeID
WHERE (VICTIMAGE>=30 AND VICTIMAGE<=35)


---11. Find persons involved in incidents of the same type as 'Robbery'.
SELECT VictimName,IncidentType
FROM Victim v 
JOIN Crime c ON v.CrimeID = c.CrimeID 
WHERE c.IncidentType = 'Robbery'

---12.List incident types with more than one open case.
SELECT IncidentType 
FROM Crime 
WHERE Status = 'Open' 
GROUP BY IncidentType 
HAVING COUNT(Status) >= 1

---13. List all incidents with suspects whose names also appear as victims in other incidents.
SELECT * 
FROM Crime c 
WHERE EXISTS (
    SELECT 1
    FROM Suspect s 
    JOIN Victim v ON s.Name = v.VictimName 
    WHERE s.CrimeID = c.CrimeID
)

---14. Retrieve all incidents along with victim and suspect details.
SELECT *, v.VictimName , s.Name  [Suspect Name]
FROM Crime c
JOIN Victim v ON c.CrimeID = v.CrimeID
JOIN Suspect s ON c.CrimeID = s.CrimeID

--15. Find incidents where the suspect is older than any victim.
---16. Find suspects involved in multiple incidents.
SELECT *FROM Suspect
SELECT Name 
FROM Suspect
GROUP BY Name
HAVING COUNT(*)>1

---17. List incidents with no suspects involved.
SELECT * 
FROM Crime 
WHERE CrimeID NOT IN (SELECT DISTINCT CrimeID FROM Suspect)

---18. List all cases where at least one incident is of type 'Homicide' and all other incidents are of type 'Robbery'.
SELECT * 
FROM Crime 
WHERE IncidentType = 'Homicide' 
OR (IncidentType = 'Robbery' AND NOT EXISTS (SELECT 1 FROM Crime WHERE IncidentType <> 'Robbery'))

--19. Retrieve a list of all incidents and the associated suspects, showing suspects for each incident, or 'No Suspect' if there are none.
SELECT c.CrimeID, IncidentType, IncidentDate, CrimeLocation, c.Description, 
       COALESCE(Name, '-No Suspect')  [Suspect Name]
FROM Crime c
LEFT JOIN Suspect s ON c.CrimeID = s.CrimeID

---20. List all suspects who have been involved in incidents with incident types 'Robbery' or 'Assault'.
SELECT *
FROM Suspect s
JOIN Crime c ON s.CrimeID = c.CrimeID
WHERE c.IncidentType IN ('Robbery', 'Assault')





